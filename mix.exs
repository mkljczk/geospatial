defmodule Geospatial.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/mkljczk/geospatial"

  def project do
    [
      app: :geospatial,
      version: "0.3.1",
      elixir: "~> 1.11",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      elixirc_paths: elixirc_paths(Mix.env()),
      aliases: aliases()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:tesla, "~> 1.11.0"},
      {:geo, "~> 3.6.0"},
      {:tz_world, "~> 1.3.2"},
      {:mox, "~> 1.1.0", only: :test},
      {:hackney, "~> 1.20.1"},
      {:ex_doc, "~> 0.30.9", only: :dev, runtime: false}
    ]
  end

  defp package() do
    [
      description:
        "This library extracts the Mobilizon.Service.Geospatial module from Mobilizon.",
      maintainers: ["Marcin Mikołajczak"],
      licenses: ["AGPL-3.0-only"],
      links: %{
        "GitLab" => @source_url
      }
    ]
  end

  defp aliases do
    [
      test: [
        "tz_world.update",
        "test"
      ]
    ]
  end

  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]
end
